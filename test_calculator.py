
def add(first_term, second_term):
    return first_term + second_term


def subtract(first_term, second_term):
    return first_term - second_term


def test_addition():
    assert 4 == add(2, 2)


def test_subtraction():
    assert 2 == subtract(4, 2)

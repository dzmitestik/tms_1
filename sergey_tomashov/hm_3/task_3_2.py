# Напишите программу, которая добавляет ‘ing’ к словам

str_base = 'aa bbb ccc'
str_to_list = str_base.split()
str_ing = 'ing '.join(str_to_list) + 'ing'

'''
One-line solution
str = 'ing '.join(str_base.split()) + 'ing'
'''

print(str_ing)

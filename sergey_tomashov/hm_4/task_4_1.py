"""
Перевести строку в массив
"Robin Singh" => ["Robin”, “Singh"]
"I love arrays they are my favorite" =>
["I", "love", "arrays", "they", "are", "my", "favorite"]

"""
str_given2 = "Robin Singh"
str_given = "I love arrays they are my favorite"
list_out = str_given.split()
list_out2 = str_given2.split()
print(list_out, list_out2)

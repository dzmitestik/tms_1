"""
*2) Дан текст, который содержит различные английские буквы и знаки препинания.
Вам необходимо найти самую частую букву в тексте.
Результатом должна быть буква в нижнем регистре.
При поиске самой частой буквы, регистр не имеет значения,
так что при подсчете считайте, что
"A" == "a". Убедитесь, что вы не считайте знаки препинания,
цифры и пробелы, а только буквы.
Если в тексте две и больше буквы с одинаковой частотой,
тогда результатом будет буква, которая идет
первой в алфавите. Для примера, "one" содержит "o", "n", "e"
по одному разу, так что мы выбираем "e".
"""

from string import punctuation, digits, whitespace

text = 'aaaa,  , bbbb. ccc vvvvvvv 875 !!!!!!!!!6'
text = text.lower()
new_text = ''
unneeded_symbols = punctuation + digits + whitespace
'''
In v1 this variable was used instead of import string
un_symbols = ['.', ',', '!', '?', ':', '"', '\'', ':', ';', ' ',
              '1', '2', '3', '4', '5', '6', '7', '8', '9', '0']
'''

for i in text:
    if i not in unneeded_symbols:
        new_text += i

new_text = ''.join(sorted(new_text))
max_symbol = new_text[0]
for i in new_text:
    if new_text.count(i) > new_text.count(max_symbol):
        max_symbol = i

print(f'The most repeatable letter is: {max_symbol}')
